#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include "prims.h"
#include <cstdlib>

/* display function - GLUT display callback function
 *		clears the screen, draws a square, and displays it
 */

 prims * test = new prims();
 float i = 1.0;
 float point1[2] = {0.0f,0.0f};
 float point2[2]={0.0,1.0};
 float point3[2]={1.0,1.0};
 float point4[2]={1.0,0.0};
 float eyes[3]={30.0, 30.0, 30.0};
 int shape = 99;
 int count = 0;

 float color1[3]={1.0,0.0,0.0};
 int LMB = 0;
 int n = 200;




void init(void)
{

    glFrontFace(GL_CW);
    glCullFace(GL_FRONT);
    glEnable(GL_CULL_FACE);
    glViewport(0, 0, 300, 300);
    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

    //glOrtho(-300,300,-300,300,-300,300);
	gluPerspective(45.0f, 1.0f, 1.0f, 100.0f);


    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_DEPTH_TEST);


	glClearColor(0,0,0,0);
    }

void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(eyes[0], eyes[1], eyes[2],//eyes
              0.0, 0.0, 0.0, //looking at
              0.0, 1.0, 0.0);//up direction


	glColor3f(1.0, 0.0, 0.0);
    test->drawBox(point1, 20.0, 1.0f, 20.0f);
    glColor3f(1.0,1.0,0.0);
    test->drawBox(point2, 2.0, 2.0f, 2.0f);
    glutSwapBuffers();
                //glutPostRedisplay();
                //glFlush();
}


void spcFunc(int key, int x, int y)
{

    switch (key)
    {
        case GLUT_KEY_UP:
            eyes[0]+=1.0f;
            break;

        case GLUT_KEY_DOWN:
            eyes[0]-=1.0f;
            break;

        case GLUT_KEY_LEFT:
            eyes[2]-=1.0f;
            break;

        case GLUT_KEY_RIGHT:
            eyes[2]+=1.0f;
            break;
    }
    glutPostRedisplay();
}

void kbdFunc(unsigned char key, int x, int y) {

	if (key == 'q')
		exit(0);
	else
	{

		if (key == 'G')
		{
			glColor3f(0.0,1.0,0.0);///colors are always reset in display func.  make new method to set colors
		}
		if (key == 'B')
		{
			glColor3f(0.0,0.0,1.0);
				//glutPostRedisplay();
		}
	}
}




void idle()
{
        	//glutSwapBuffers();
            glutPostRedisplay();
}

/* main function - program entry point */
int main(int argc, char** argv)
{
	glutInit(&argc, argv);		//starts up GLUT
	glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);

	glutCreateWindow("square");	//creates the window
	glutDisplayFunc(display);	//registers "display" as the display callback function
	glutIdleFunc(idle);
	glutKeyboardFunc(kbdFunc);
	glutSpecialFunc(spcFunc);
	//glutMotionFunc(motion);
	init();
	glutMainLoop();				//starts the event loop

	return(0);					//return may not be necessary on all compilers
}
